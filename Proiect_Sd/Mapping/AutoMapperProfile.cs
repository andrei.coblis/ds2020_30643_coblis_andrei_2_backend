﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.Entities;

namespace Proiect_Sd.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<UserRequestDTO, User>();

            CreateMap<PatientRequestDTO, Patient>();

            CreateMap<CaregiverRequestDTO, Caregiver>();

            CreateMap<MedicationRequestDTO, Medication>();

            CreateMap<DoctorRequestDTO, Doctor>();

            CreateMap<MedicationPlanRequestDTO, MedicationPlan>();
        }
    }
}
