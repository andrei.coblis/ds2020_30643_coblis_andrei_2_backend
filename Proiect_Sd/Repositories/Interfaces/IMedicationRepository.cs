﻿using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Repositories.Interfaces
{
    public interface IMedicationRepository
    {
        Task<List<MedicationResponseDTO>> GetMedications();
        Task<MedicationResponseDTO> GetMedicationResponseById(int medicationId);
        Task<Medication> GetMedicationById(int medicationId);
        Task<Medication> CreateMedication(Medication medication);
        Task<bool> UpdateMedication(Medication medication);
        Task<bool> CheckIfMedicationExists(int medicationId);
        Task<bool> DeleteMedication(Medication medication);
    }
}
