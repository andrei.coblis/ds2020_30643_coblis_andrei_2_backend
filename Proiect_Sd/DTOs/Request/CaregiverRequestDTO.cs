﻿using System.ComponentModel.DataAnnotations;

namespace Proiect_Sd.DTOs.Request
{
    public class CaregiverRequestDTO
    {
        [Required(ErrorMessage = "Please enter an user ID")]
        public int UserId { get; set; }
    }
}
