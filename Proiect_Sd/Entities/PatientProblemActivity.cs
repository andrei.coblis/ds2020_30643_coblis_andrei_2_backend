﻿namespace Proiect_Sd.Entities
{
    public class PatientProblemActivity
    {
        public int PatientId { get; set; }
        public string Activity { get; set; }
    }
}
