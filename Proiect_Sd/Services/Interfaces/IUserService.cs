﻿using Proiect_Sd.DTOs.Request;
using Proiect_Sd.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetUsers();
        Task<User> GetUserByIdAsync(int userId);
        Task<User> CreateUserAsync(UserRequestDTO userRequestDTO);
        Task<bool> UpdateUserAsync(UserRequestDTO userRequestDTO, int userId);
        Task<bool> CheckIfUserExistsAsync(int userId);
        Task<bool> DeleteUserAsync(int userId);
    }
}
