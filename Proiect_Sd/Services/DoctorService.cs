﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using Proiect_Sd.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services
{
    public class DoctorService : IDoctorService
    {
        private readonly IDoctorRepository _doctorRepository;
        private readonly IMapper _mapper;

        public DoctorService(IDoctorRepository doctorRepository, IMapper mapper)
        {
            _doctorRepository = doctorRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfDoctorExistsAsync(int doctorId)
        {
            return await _doctorRepository.CheckIfDoctorExists(doctorId);
        }

        public async Task<Doctor> CreateDoctorAsync(DoctorRequestDTO doctorRequestDTO)
        {
            var doctor = _mapper.Map<Doctor>(doctorRequestDTO);
            return await _doctorRepository.CreateDoctor(doctor);
        }

        public async Task<bool> DeleteDoctorAsync(int doctorId)
        {
            var doctor = await GetDoctorByIdAsync(doctorId);

            return await _doctorRepository.DeleteDoctor(doctor);
        }

        public async Task<Doctor> GetDoctorByIdAsync(int doctorId)
        {
            return await _doctorRepository.GetDoctorById(doctorId);
        }

        public async Task<DoctorResponseDTO> GetDoctorResponseByIdAsync(int doctorId)
        {
            return await _doctorRepository.GetDoctorResponseById(doctorId);
        }

        public async Task<List<DoctorResponseDTO>> GetDoctors()
        {
            return await _doctorRepository.GetDoctors();
        }

        public async Task<bool> UpdateDoctorAsync(DoctorRequestDTO doctorRequestDTO, int doctorId)
        {
            var doctor = _mapper.Map<Doctor>(doctorRequestDTO);
            doctor.DoctorId = doctorId;

            return await _doctorRepository.UpdateDoctor(doctor);
        }
    }
}
