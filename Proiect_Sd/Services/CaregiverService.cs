﻿using AutoMapper;
using Proiect_Sd.DTOs.Request;
using Proiect_Sd.DTOs.Response;
using Proiect_Sd.Entities;
using Proiect_Sd.Repositories.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Proiect_Sd.Services.Interfaces
{
    public class CaregiverService : ICaregiverService
    {
        private readonly ICaregiverRepository _caregiverRepository;
        private readonly IMapper _mapper;

        public CaregiverService(ICaregiverRepository caregiverRepository, IMapper mapper)
        {
            _caregiverRepository = caregiverRepository;
            _mapper = mapper;
        }

        public async Task<bool> CheckIfCaregiverExistsAsync(int caregiverId)
        {
            return await _caregiverRepository.CheckIfCaregiverExists(caregiverId);
        }

        public async Task<Caregiver> CreateCaregiverAsync(CaregiverRequestDTO caregiverRequestDTO)
        {
            var caregiver = _mapper.Map<Caregiver>(caregiverRequestDTO);
            return await _caregiverRepository.CreateCaregiver(caregiver);
        }

        public async Task<bool> DeleteCaregiverAsync(int caregiverId)
        {
            var caregiver = await GetCaregiverByIdAsync(caregiverId);

            return await _caregiverRepository.DeleteCaregiver(caregiver);
        }

        public async Task<Caregiver> GetCaregiverByIdAsync(int caregiverId)
        {
            return await _caregiverRepository.GetCaregiverById(caregiverId);
        }

        public async Task<CaregiverResponseDTO> GetCaregiverResponseByIdAsync(int caregiverId)
        {
            return await _caregiverRepository.GetCaregiverResponseById(caregiverId);
        }

        public async Task<List<CaregiverResponseDTO>> GetCaregivers()
        {
            return await _caregiverRepository.GetCaregivers();
        }

        public async Task<bool> UpdateCaregiverAsync(CaregiverRequestDTO caregiverRequestDTO, int caregiverId)
        {
            var caregiver = _mapper.Map<Caregiver>(caregiverRequestDTO);
            caregiver.CaregiverId = caregiverId;

            return await _caregiverRepository.UpdateCaregiver(caregiver);
        }
    }
}
